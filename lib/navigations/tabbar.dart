import 'package:flutter/material.dart';

import '../views/home.dart';
import '../views/library.dart';
import '../views/profile.dart';
import '../views/search.dart';

class Tabbar extends StatefulWidget {
  const Tabbar({super.key});

  @override
  State<Tabbar> createState() => _TabbarState();
}

class _TabbarState extends State<Tabbar> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.library_music),
            label: 'Your library',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Pofile',
          ),
        ],
      ),
      body: Stack(
        children: [
          renderView(0, const HomeView()),
          renderView(1, const SearchView()),
          renderView(2, const LibraryView()),
          renderView(3, const ProfileView()),
        ],
      ),
    );
  }

  Widget renderView(int tabIndex, Widget view) {
    return IgnorePointer(
      ignoring: _selectedIndex != tabIndex,
      child: Opacity(opacity: _selectedIndex == tabIndex ? 1 : 0, child: view),
    );
  }
}
